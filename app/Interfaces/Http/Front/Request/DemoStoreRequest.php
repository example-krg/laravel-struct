<?php
declare(strict_types=1);
namespace App\Interfaces\Http\Front\Request;

use Illuminate\Foundation\Http\FormRequest;

class DemoStoreRequest extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string|max:190',
        ];
    }
}
