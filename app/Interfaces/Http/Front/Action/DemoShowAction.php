<?php
declare(strict_types=1);
namespace App\Interfaces\Http\Front\Action;

use App\Infrastructure\Commands\Demo\DemoGetByIdCommand;
use App\Interfaces\Http\BaseAction;

class DemoShowAction extends BaseAction
{
    private DemoGetByIdCommand $demoGetByIdCommand;

    public function __construct(DemoGetByIdCommand $demoGetByIdCommand)
    {
        $this->demoGetByIdCommand = $demoGetByIdCommand;
    }

    public function __invoke(int $id)
    {
        $demo = $this->demoGetByIdCommand->execute($id);

        if(!$demo){
            return back()->withErrors('errors', trans('front.error_XXX'));
        }

        return view('welcome');
    }
}
