<?php
declare(strict_types=1);
namespace App\Interfaces\Http\Front\Action;

use App\Infrastructure\Commands\Demo\DemoStoreCommand;
use App\Interfaces\Http\BaseAction;
use App\Interfaces\Http\Front\Request\DemoStoreRequest;

class DemoStoreAction extends BaseAction
{
    private DemoStoreCommand $demoStoreCommand;

    public function __construct(DemoStoreCommand $demoStoreCommand)
    {
        $this->demoStoreCommand = $demoStoreCommand;
    }

    public function __invoke(DemoStoreRequest $request)
    {
        $data = $request->validated();
        $this->demoStoreCommand->execute($data);

        return redirect()->route('XXX')->with('message', trans('front.message_XXX'));
    }
}
