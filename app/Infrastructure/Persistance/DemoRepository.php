<?php
declare(strict_types=1);
namespace App\Infrastructure\Persistance;

use App\Domain\Contracts\DemoContract;
use App\Domain\Models\Demo;

class DemoRepository implements DemoContract
{
    public function store(array $data): void
    {
        Demo::create($data);
    }

    public function getById(int $id): ?Demo
    {
        return Demo::find($id);
    }

}
