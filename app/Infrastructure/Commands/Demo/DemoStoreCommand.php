<?php
declare(strict_types=1);
namespace App\Infrastructure\Commands\Demo;

use App\Domain\Contracts\DemoContract;

class DemoStoreCommand
{
    private DemoContract $contract;

    public function __construct(DemoContract $contract)
    {
        $this->contract = $contract;
    }

    /**
     * @param array $data
     * @return void
     */
    public function execute(array $data):void
    {
        $this->contract->store($data);
    }
}
