<?php
declare(strict_types=1);
namespace App\Infrastructure\Commands\Demo;

use App\Domain\Contracts\DemoContract;
use App\Domain\Models\Demo;

class DemoGetByIdCommand
{
    private DemoContract $contract;

    public function __construct(DemoContract $contract)
    {
        $this->contract = $contract;
    }

    /**
     * @param int $id
     * @return Demo|null
     */
    public function execute(int $id): ?Demo
    {
        return $this->contract->getById($id);
    }
}
