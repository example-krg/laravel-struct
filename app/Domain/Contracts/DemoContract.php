<?php
declare(strict_types=1);
namespace App\Domain\Contracts;

use App\Domain\Models\Demo;

interface DemoContract
{
    public function store(array $data): void;

    public function getById(int $id): ?Demo;
}
