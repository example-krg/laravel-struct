<?php
declare(strict_types=1);
namespace App\Domain\Models;

use Illuminate\Database\Eloquent\Model;

class Demo extends Model
{
    protected $table = 'demos';

    protected $fillable = [
        'name'
    ];
}
